# *Why Databricks*

An overview of the benefits towards moving to a cloud-based Big Data architecture.

&nbsp;
## Table of Contents

- [Glossary of Terms](#glossary)
- [What Is Databricks?](#what-problem-does-databricks-solve?)
- [Benefits](#benefits)
- [Scaling](#scaling)
- [Cutting Down Datorama Row Usage](#cutting-down-row-usage-datorama)
- [Comparison To Current Process](#how-does-it-compare-to-datorama-pipeline)
- [Expected Cost](#expected-cost)

&nbsp;
### Glossary

- `Databricks`: Manages Spark clusters and provides a clean user interface specifically geared for highly performant and collaborative data analysis.

	> A product that aims to help clients with cloud-based big data processing using Spark. Users achieve faster time-to-value with Databricks by creating analytic workflows that go from ETL and interactive exploration to production. The company also makes it easier for its users to focus on their data by providing a fully managed, scalable, and secure cloud infrastructure that reduces operational complexity and total cost of ownership.

- `Spark`: Run analytics across massive distributed datasets efficiently.

	> Also called Apache Spark, is a powerful open source processing engine built around speed, ease of use, and sophisticated analytics.

- `Amazon S3`: Infinite file storage (any type, any amount, any size). 

	> A web service from Amazon Web Services (AWS). Amazon S3 provides *infinitely* scalable storage infrastructure. S3 is comprised of two constructs: buckets and objects. Buckets are the top level containters that store objects.

- `Cluster`: A way of structuring computing resources for maximum efficiency.

	> A computer cluster is a set of loosely, or tightly, connected computers that work together so that, in many respects, they can be viewed as a single system.

&nbsp;
A basic diagram explaining each services role:
```text
					  ---------------------
					  |    DATABRICKS     |  --> Car (write code, visualizations, insights)
					  ---------------------
					  |       SPARK       |  --> Engine (runs computations on data)
----------------	  ---------------------
| Data Sources | -->  |        S3         |  --> Fuel (stores files, data)
----------------	  ---------------------
```

&nbsp;
### What Problem Does Databricks Solve?

Databricks exists to make cloud-based big data processing easy. They achieve this promise through utilizing a technology called Apache Spark - something the Databricks team had a hand in creating. Before diving into what each service is, and how they relate, some context regarding the issue they are trying to mitigate will prove helpful.

For some perspective, you generate, and use, more data today from your phone than entire countries did in the early 1990s. This is at the core of the problem: with so much data being generated, at increasingly faster speeds, how can we extract out everything useful?

&nbsp;
![bigdatabigproblems](https://i.imgur.com/AHym0mf.jpg)
&nbsp;

Amazon S3 is the first step in this adventure. S3 serves as a home to store an infinite amount of data in an efficient manner. Specifically, for around $23 dollars a month you can hold a Terabyte worth of data. How much is a Terabyte of data? Roughly speaking if you streamed 416 Netflix videos, each 90 minutes long, you'd have hit 1 Terabyte in overall data usage.[1]

Spark is the second step, and more specifically the engine. Your computer, similarly to your phone, has a constrained amount of memory in regards to the amount of data it can hold. If we wanted to sum up the cost of every Crossmedia campaign ever run that wouldn't be feasible on a computer due to memory limitations. In that scenario, the file (stored in Amazon S3) containing said historical cost data may be in excess of 2 Terabytes, loosely 2048 gb (the memory capacity of 32 top of the line 64gb iPhones). It simply isn't feasible to run computations on such a larger dataset using modern computers. Therefore, Spark breaks this process up over numerous `clusters`.

These clusters exist across numerous machines making them distributed and capable of holding such large data. Futhermore, Spark makes asking questions like the former easier because it's constructed to run large scale computations in parallel across enormous datasets with ease.

The tricky part with Spark is managing these clusters and how they get access to the data. The typical workflow is demonstrated below.

&nbsp;
![bigdatahard](https://i.imgur.com/sYOOBoB.png)
&nbsp;

Databricks steps in to handle the DevOps burden associated with cluster managmenent and big data infastructure. Specifically, Databricks streamlines the process of grabbing data from Amazon S3 and making it readily available for anyone to ask questions.

&nbsp;
![datayeah](https://i.imgur.com/E8vwIGu.png)
&nbsp;

The example below demonstrates one of the core features of Databricks: *notebooks*.

&nbsp;
![notebook](https://i.imgur.com/7wfeyLR.png)
![notebook2](https://i.imgur.com/si8KoJv.png)
&nbsp;

The important thing to note is that Databricks, and Spark underneath the hood, are all about building an infrastructure for Big Data that is easy to maintain and scalable. 

&nbsp;
![databrikz](https://i.imgur.com/B0wv52B.png)

&nbsp;
### Benefits

#### Optimize Ad Creative

> Digital advertising relies on personalized engagement, and with seemingly infinite variations in headline copy, visual creative, size, placement, etc.., delivering the right content to the ideal customer can quickly get complex. Machine algorithms can instatnly identify which combinations of ad creative and placement are most likely to predict engagement, as well as which types of advertisements resonate with different buyer personas.

#### Aggregate Audience Data to Refine Audience Targeting

> By sifting through aggregate ad performance data, which includes impressions, clicks, conversions, etc...You can employ machine learning to identify actionable information, such as engagement signals and buying patterns. Then you can use these signals and patterns to identify which products certain audience clusters prefer; when, how, and where they like to buy, and with which types of ads they engage.

#### Automate Campaign Decisions

> Automated campaign decision-making is a growing trend in digital advertising that employs machine learning in a real-time environment to reveal hidden insights about performance and automatically optimize campaigns. Through automated campaign decisions, advertisers can reduce irrelevant data analysis.

#### Launch Point for Advanced Analytics

> Advertising data is rich with opportunities for exploration. Gaining fast and flexible access to all your data can open the door to new product opportunities, customer acquisition strategies, and industry-wide innovation.

&nbsp;
### Scaling

Databricks' advanced Spark cluster manager allows you to scale up fully managed Spark clusters according to your data processing needs. In addition to making it easy to scale up, the cluster manager also helps you scale down or terminate clusters when they are not in use to optimize resource allocation and spend.

> Therefore, by delivering a fast, scalable and flexible data processing platform, Databricks allows you to produce real-time reports to customers, scale growing datasets, and explore the rich insights held in your advertising data.

&nbsp;
### Cutting Down Row Usage Datorama

With Databricks row usage in Datorama shrinks from simply not needing to store everything. With Databricks and Amazon S3, Datorama would only need to store the data that directly pertains to each dashboard - no more, no less. Additionally, the burden of ETL'ing the data is shifted to an environment that is more suitable for such an endeavor.

&nbsp;
### How Does It Compare to Datorama Pipeline

In comparison to Datorama, Databricks + Spark is the industry standard. Data analysis, and ETL, are conducted in notebook environments supporting multiple languages. Additional benefits are code re-use and version control for ensuring top notch integrity of both the code and data.

The languages available to conduct analysis in Databricks are accessible to all. On the beginner side there is SQL, allowing for straight forward data interaction utilizing a declarative paradignm. Conversely, R and Python are great for intermediate analysts looking to take the next step in their career. And on the high end, Java and Scala are available to write statically typed code, ensuring further safety, for production products.

Databricks will make data analysis more collaborative, while also helping everyone to up their techincal chops.

&nbsp;
### Expected Cost

[1]: https://www.consumerreports.org/telecom-services/how-easy-to-burn-through-1TB-data-cap/
