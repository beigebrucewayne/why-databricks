# Costs

- [Databricks - ML](#databricks-ml)
- [Databricks - ETL](#databricks-etl)
- [Glue - ETL](#glue-etl)


## Databricks ML

10 r3.xlarge nodes       |
--                       |
x 4 hours                |
x 1 DBU per hour         |
x $0.40 per DBU          |
x 30 days                |
total:                   |
$ 480                    |
node costs:              |
+ $0.3194 * (# of nodes) |
total:                   |
$ 383                    |
`final cost:`            |
`$ 863`                  |


## Databricks ETL

10 c3.4xlarge nodes     |
--                      |
x 2 hours               |
x 2 DBUs per hour       |
x $0.40 per DB*U         |
x 30 days               |
total:                  |
$ 480                   |
node costs:             |
+ $0.556 * (# of nodes) |
total:                  |
$ 667                   |
`final cost:`           |
`$ 1147`                |


## Glue ETL


10 DPUs          |
--               |
x 2 hours        |
x $0.44 per DPU  |
x 30 days        |
`final cost:`    |
`$ 264`          |
